def is_triangle(a, b, c):
    if a > 0 and b > 0 and c > 0:
        max_side = max(a, b, c)
        sum_others = a + b + c - max_side
        return max_side < sum_others
    else:
        return False
